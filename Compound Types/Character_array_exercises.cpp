#include "stdafx.h"
#include <iostream>

void show_string_capacity();
void character_array_with_cin_input();
void character_array_with_cin_getline();
void character_array_with_cin_get();
void character_array_with_cin_get_corrected();
void character_array_newline_after_int_read_problem();

int main_1()
{
	//show_string_capacity();
	//character_array_with_cin_input();
	//character_array_with_cin_getline();
	//character_array_with_cin_get();
	//character_array_with_cin_get_corrected();
	//character_array_newline_after_int_read_problem();

	//input is blocked after get reads an empty line it sets a failbit 
	//and input is blocked and you have to clear it with cin.clear
	//when the input is longed than the size of the array both get/getline
	//leave the remaining characters in the input queue but getline sets the failbit
	//automatically

	return 0;
}

void show_string_capacity()
{
	using std::cout;
	using std::endl;

	char string[10] = "banana";

	cout << "Size of the whole array: " << sizeof string << endl;
	cout << "Size of the string itself: " << strlen(string) << endl;
	cout << "Null character/End of the string: string[6] = \\0" << string[6] << endl;
	cout << "Memory size of the first element: " << sizeof(*string) << endl;
	cout << "Replace 3rd element with \\0" << endl;
	string[2] = '\0';
	cout << "Size of string: " << strlen(string) << endl;
	cout << "Remaining characters are ignored." << endl;
}

void character_array_with_cin_input()
{
	using std::cin;
	using std::cout;

	//Works as long as you give single word input.
	//When you have more than 1 word inpout cin will write the first word
	//in the first array and the second will be left in the input queue.
	//In this example it will go to dessert.

	//Another problem that can occure is that when the input is longer than our
	//array, and only those that fit will be saved, in this case the first 19 character.
	//The rest will be written in memory in the next available slot but we have no control
	//what happens with them.

	const int array_size = 20;
	char name[array_size];
	char dessert[array_size];

	cout << "Enter your name:\n";
	cin >> name;
	cout << "Enter your favourite dessert:\n";
	cin >> dessert;
	cout << "I have some delicious " << dessert;
	cout << " for you, " << name << ".\n";
}

void character_array_with_cin_getline()
{
	using std::cin;
	using std::cout;

	const int array_size = 20;
	char name[array_size];
	char dessert[array_size];

	//reads no more than the size given
	//replaces new line character with null character

	cout << "Enter your name:\n";
	cin.getline(name, array_size);
	cout << "Enter your favourite dessert:\n";
	cin.getline(dessert, array_size);
	cout << "I have some delicious " << dessert;
	cout << " for you, " << name << ".\n";

}

void character_array_with_cin_get()
{
	using std::cin;
	using std::cout;

	const int array_size = 20;
	char name[array_size];
	char dessert[array_size];

	//the first call leaves the newline character in the input queue, that newline character is
	//the first character the second call sees, leaving the newline character to the second string,
	//leaving it empty.

	cout << "Enter your name:\n";
	cin.get(name, array_size);
	cout << "Enter your favourite dessert:\n";
	cin.get(dessert, array_size);
	cout << "I have some delicious " << dessert;
	cout << " for you, " << name << ".\n";

}

void character_array_with_cin_get_corrected()
{
	using std::cin;
	using std::cout;

	const int array_size = 20;
	char name[array_size];
	char dessert[array_size];

	//calling get without parameters will take a single character
	//like this we will avoid the problem with new line

	cout << "Enter your name:\n";
	cin.get(name, array_size).get();
	cout << "Enter your favourite dessert:\n";
	cin.get(dessert, array_size).get();
	cout << "I have some delicious " << dessert;
	cout << " for you, " << name << ".\n";
}

void character_array_newline_after_int_read_problem()
{
	using std::cin;
	using std::cout;
	using std::endl;

	cout << "What years was your house built?\n";
	int year;
	//problem here is that after cin reads the year from the input the newline
	//character is left in the input queue
	cin >> year;
	//cin.get() fixed with a simple get call
	cout << "What is its street address?\n";
	const int array_size = 80;
	char address[array_size];
	cin.getline(address, array_size);
	cout << "Year built : " << year << endl;
	cout << "Address: " << address << endl;
	cout << "Done!\n";

}