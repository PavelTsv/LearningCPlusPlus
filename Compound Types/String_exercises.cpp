#include "stdafx.h"
#include <iostream>
#include <string>

void give_deserved_grade();
void find_favourite_dessert();
void print_both_names();
void print_both_names_string();
void concat_two_strings();
void display_candy_bar();
void display_array_of_candy_bars();
void display_pizza_delivery();

int main6()
{
	//give_deserved_grade();
	//find_favourite_dessert();
	//print_both_names();
	//print_both_names_string();
	//concat_two_strings();
	//display_candy_bar();
	//display_array_of_candy_bars();
	//display_pizza_delivery();


	return 0;
}

void give_deserved_grade()
{
	using std::cin;
	using std::cout;
	using std::endl;

	const int max_name_size = 20;

	char first_name[max_name_size];
	char last_name[max_name_size];
	char grade;
	char deserved_grade = 'C';
	int age;

	cout << "What is your first name? ";
	cin.getline(first_name, max_name_size - 1);
	cout << "What is your last name? ";
	cin.getline(last_name, max_name_size - 1);
	cout << "What letter grade do you deserve? ";
	cin.get(grade);
	cout << "What is your age? ";
	cin >> age;

	cout << "Name: " << last_name << ", " << first_name << endl;
	cout << "Grade: " << deserved_grade << endl;
	cout << "Age: " << age << endl;

}

void find_favourite_dessert()
{
	using std::string;
	using std::cout;
	using std::cin;

	string name;
	string dessert;

	cout << "Enter your name:\n";
	getline(cin, name);
	cout << "Enter your favourite dessert:\n";
	getline(cin, dessert);
	cout << "I have some delicious " << dessert;
	cout << " for you, " << name << ".\n";

}

void print_both_names()
{
	using std::cin;
	using std::cout;
	using std::endl;

	const int max_name_size = 20;

	char first_name[max_name_size];
	char last_name[max_name_size];
	char *in_a_single_string;

	cout << "Enter your first name: ";
	cin >> first_name;
	cin.ignore(10000, '\n');
	cout << "Enter your last name: ";
	cin >> last_name;
	cin.ignore(10000, '\n');
	in_a_single_string = new char[strlen(first_name) + strlen(last_name) + 1];
	//strcpy(in_a_single_string, first_name);
	//strcpy(in_a_single_string, ", ");
	//strcpy(in_a_single_string, last_name);

	cout << in_a_single_string << endl;


}

void print_both_names_string()
{
	using std::cout;
	using std::cin;
	using std::string;
	using std::endl;

	string first_name;
	string last_name;
	string both_names;

	cout << "Enter first name: ";
	cin >> first_name;
	cout << "Enter last name: ";
	cin >> last_name;
	both_names = first_name + ", " + last_name;
	cout << "Here\'s the information in a single string: " << both_names << endl;


}

void concat_two_strings()
{
	using std::cin;
	using std::cout;
	using std::endl;

	const int name_size = 20;
	char first_name[name_size];
	char last_name[name_size];

	cout << "Enter your first name: ";
	cin.getline(first_name, strlen(first_name));
	cout << "Enter your last name: ";
	cin.getline(last_name, strlen(last_name));
	char additional_string[] = ", ";
	const int both_names_size = 41;
	char both_names[both_names_size];
	//strcpy(both_names, last_name);
	//strcat(both_names, additional_string);
	//strcat(both_names, first_name);

	cout << "Here is the information in a single string: " << both_names << endl;
}

void display_candy_bar()
{
	using std::string;
	using std::cout;
	using std::endl;

	struct CandyBar
	{
		string brand_name;
		float weight;
		int number_of_calories;
	};

	CandyBar snack = {
		"Mocha Munch",
		2.3,
		350
	};

	cout << "Brand name: " << snack.brand_name << endl;
	cout << "Snack weights: " << snack.weight << endl;
	cout << "Number of calories: " << snack.number_of_calories << endl;
}

void display_array_of_candy_bars()
{
	using std::string;
	using std::cout;
	using std::endl;

	struct CandyBar
	{
		string brand_name;
		float weight;
		int number_of_calories;
	};

	CandyBar candy_bars[3];

	candy_bars[0] = {
		"A",
		3.5,
		250
	};

	candy_bars[1] = {
		"B",
		2,
		100
	};

	candy_bars[2] = {
		"C",
		7.5,
		1000
	};
	cout << candy_bars[0].brand_name << ", " << candy_bars[1].brand_name << ", " << candy_bars[2].brand_name << endl;

}

void display_pizza_delivery()
{
	using std::cout;
	using std::cin;
	using std::endl;
	using std::string;

	struct Pizza
	{
		string company_name;
		int pizza_diameter;
		float weight;
	};

	Pizza* pizza = new Pizza;

	cout << "Pizza diameter: ";
	cin >> pizza->pizza_diameter;
	cout << "Pizza company name: ";
	cin >> pizza->company_name;
	cout << "Pizza weights: ";
	cin >> pizza->weight;

	cout << pizza->company_name + ", " << pizza->pizza_diameter << ", " << pizza->weight << endl;

	delete pizza;
}