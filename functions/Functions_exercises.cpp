#include "stdafx.h"
#include <iostream>
#include <string>

void reference_exercises();
void print_times();
void print_string(std::string, int times = 1);
struct CandyBar;
void set_CandyBar(CandyBar& bar, std::string brand = "Millennium Munch", float weight = 2.85, int calories = 350);
void use_CandyBar();

int main20()
{
	//reference_exercises();
	use_CandyBar();
	return 0;
}

void reference_exercises()
{
	using std::cout;
	using std::endl;

	int number = 5;
	int& reference_to_number = number;

	cout << "Ref to number value: " << reference_to_number << endl;
	cout << "Number values: " << reference_to_number << endl;
	cout << "Address of number: " << &number << endl;
	cout << "Address of reference: " << &reference_to_number << endl;
	//changing the value of the reference changes also the value of number
	//you cant change where the reference is pointing too
	//it is like a int* const pointer
	int number_two = 10;
	reference_to_number = number_two;
	cout << "Number: " << number << " Reference: " << reference_to_number << " Address of number: " << &number << " Address of reference: " << &reference_to_number << endl;

	int rats = 101;
	int * pt = &rats;
	int & rodents = *pt;
	int bunnies = 50;
	pt = &bunnies;
	//rodents still points to pt
	//const double &a cant change the value
	//be careful with temporary variable aka when a function requires a reference to double for example and it is given int it i will create
	//a temporary variable instead of using the reference given.
	
	//returning a reference doesn't make a copy of the value instead it returns the value
	//dont return references to variables that seize to exist after function ends
	int count(int n, int k = 5); // prototype has default value of 5
}

void print_times()
{
	print_string("hello", 5);

}

int times_called = 0;

void print_string(const std::string str, const int times)
{
	for (int i = 0; i < times == 1 ? times : times_called; i++)
	{
		std::cout << "Print#" << i << " : " << str << std::endl;
	}
	times_called++;
}

struct CandyBar {
	std::string brand;
	float weight;
	int calories;
};

void set_CandyBar(CandyBar& bar, const std::string brand, const float weight, const int calories)
{
	bar.brand = brand;
	bar.weight = weight;
	bar.calories = calories;

}

void use_CandyBar()
{
	CandyBar* bar = new CandyBar;

	set_CandyBar(*bar);
	std::cout << bar->brand << ", " << bar->calories << ", " << bar->weight << std::endl;

	delete bar;
}
