﻿#pragma once

class Brass
{
	enum {max = 35};
	char full_name_[max];
	long acc_num_;
	double balance_;
public:
	explicit Brass(const char* s = "Nullbody", long an = -1, double bal = 0.0);
	void deposit(double amt);
	virtual void withdraw(double amt);
	double balance() const { return balance_; }
	virtual void view_acc() const;
	virtual ~Brass();
};

class BrassPlus : public Brass
{
	double max_loan_;
	double rate_;
	double owes_bank_;
public:
	explicit BrassPlus(const char* s = "Nullbody", long an = -1, double bal = 0.0, double ml = 500, double r = 0.10);
	explicit BrassPlus(const Brass& br, double ml = 500, double r = 0.10);
	void view_acc() const override;
	void withdraw(double amt) override;
	void reset_max(double m) { max_loan_ = m; }
	void reset_rate(double r) { rate_ = r; }
	void reset_owes() { owes_bank_ = 0; }
};