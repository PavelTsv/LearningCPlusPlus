﻿#include "stdafx.h"
#include "port.h"
#include <iostream>
#pragma warning(disable : 4996)

port::port(const char* brand, const char* style, const int bottles)
{
	brand_ = new char[strlen(brand) + 1];
	strcpy(brand_, brand);
	strncpy(style_, style, 19);
	style_[19] = '\0';
	bottles_ = bottles;
}

port::port(const port& port)
{
	brand_ = new char[strlen(port.brand_) + 1];
	strcpy(brand_, port.brand_);
	strncpy(style_, port.style_, 19);
	style_[19] = '\0';
	bottles_ = port.bottles_;
}

port& port::operator=(const port& p)
{
	if (this == &p)
		return *this;
	delete[] brand_;
	brand_ = new char[strlen(p.brand_) + 1];
	strcpy(brand_, p.brand_);
	strncpy(style_, p.style_, 19);
	style_[19] = '\0';
	bottles_ = p.bottles_;
}

port& port::operator+=(const int b)
{
	bottles_ += b;
	return *this;
}

port& port::operator-=(const int b)
{
	bottles_ -= b;
	return *this;
}

std::ostream& operator<<(std::ostream& os, const port& port)
{
	os << port.brand_ << ", " << port.style_ << ", " << port.bottles_;
	return os;
}

void port::show() const
{
	std::cout << "Brand: " << brand_;
	std::cout << "Kind: " << style_;
	std::cout << "Bottles: " << bottles_;
}


vintage_port::vintage_port()
{
	//nickname_ = new char[] {"null"};
	year_ = 0;
}

vintage_port::vintage_port(const char* nick, const int y, const char* brand, const char* style, const int bottles)
{
	
}

void vintage_port::show() const
{
	
}
