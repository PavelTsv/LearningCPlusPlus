﻿#include "stdafx.h"
#include "time.h"
#include <iostream>

time::time()
{
	hours_ = minutes_ = 0;
}

time::time(const int h, const int m)
{
	hours_ = h;
	minutes_ = m;
}

void time::add_min(const int m)
{
	minutes_ += m;
	hours_ += minutes_ / 60;
	minutes_ %= 60;
}

void time::add_hour(const int h)
{
	hours_ += h;
}

void time::reset(const int h, const int m)
{
	hours_ = h;
	minutes_ = m;
}

time time::operator+(const time& t) const
{
	time sum;
	sum.minutes_ = minutes_ + t.minutes_;
	sum.hours_ = hours_ + t.hours_ + sum.minutes_ / 60;
	sum.minutes_ %= 60;

	return sum;
}

time operator*(const double m, const time& t)
{
	return t * m;
}

time time::operator*(const double mult) const
{
	time result;
	const long totalminutes = hours_ * mult * 60 + minutes_ * mult;
	result.hours_ = totalminutes / 60;
	result.minutes_ = totalminutes % 60;

	return result;
}

std::ostream& operator<<(std::ostream& os, const time& t)
{
	os << t.hours_ << " hours, " << t.minutes_ << " minutes";

	return os;
}
