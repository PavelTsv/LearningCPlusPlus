﻿#pragma once
#include <ostream>

class port
{
	char* brand_;
	char style_[20];
	int bottles_;
public:
	explicit port(const char* brand = "none", const char* style = "none", const int bottles = 0);
	port(const port& port);
	virtual ~port() { delete[] brand_; }
	port& operator=(const port& p);
	port& operator+=(const int b);
	port& operator-=(const int b);
	int bottles() const { return bottles_; }
	virtual void show() const;
	friend std::ostream& operator<<(std::ostream& os, const port& port);

};

class vintage_port : public port
{
	char* nickname_;
	int year_;
public:
	explicit vintage_port();
	explicit vintage_port(const char* nick, const int y, const char* brand, const char* style, const int bottles);
	~vintage_port() { delete[] nickname_; }
	vintage_port& operator=(const vintage_port& vp);
	void show() const override;
	friend std::ostream& operator<<(std::ostream& os, const vintage_port& vp);
};