﻿#pragma once

class cd
{
	char performance_[50];
	char label_[20];
	int selection_;
	double play_time_;

public:
	explicit cd(const char* s1 = "null", const char* s2 = "null", int n = 0, double x = 0.0);
	virtual ~cd();
	virtual void report() const;
};

class classic : public cd
{
	char* id_;
public:
	explicit classic(char* id = "null", char* s1 = "null", char* s2 = "null", int n = 0, double x = 0.0);
	classic(const classic& c);
	~classic();
	void report() const override;
	classic& operator=(const classic& c);

};
