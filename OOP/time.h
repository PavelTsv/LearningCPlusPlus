﻿#pragma once
#include <ostream>

class time
{
	int hours_;
	int minutes_;
public:
	time();
	explicit time(const int h, const int m = 0);
	void add_min(const int m);
	void add_hour(const int h);
	void reset(const int h = 0, const int m = 0);
	time operator+(const time& t) const;
	time operator*(const double mult) const;
	friend time operator*(const double m, const time & t);
	friend std::ostream& operator<<(std::ostream& os, const time& t);
};
