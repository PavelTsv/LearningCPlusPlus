﻿#include "stdafx.h"
#include "Wine.h"
#include <iostream>
typedef std::valarray<int> a_int;
typedef Pair<a_int, a_int> p_arr;

Wine::Wine(const std::string name, const int years, const int year[], const int bottle[]) : std::string(name), p_arr(a_int(year, years), a_int(bottle, years)), number_of_years_(years)
{
	
}

Wine::Wine(const std::string name, const int year) : std::string(name), number_of_years_(year), p_arr(a_int(year), a_int(year))
{
}

std::string& Wine::name()
{
	return (std::string&) *this;
}

void Wine::get_bottles()
{
	const auto i = number_of_years_;
	do
	{
		std::cout << "Enter vintage year: ";
		std::cin >> first()[i - number_of_years_];
		std::cout << "Enter number of bottles: ";
		std::cin >> second()[i - number_of_years_];
	}
	while (--this->number_of_years_ > 0);
}

void Wine::show() const
{
	std::cout << "Vintage years: " << "\n";
	for (auto i = 0; i < first().size(); i++)
	{
		std::cout << first()[i] << std::endl;
	}
	std::cout << "Bottle count: " << "\n";
	for (auto i = 0; i < second().size(); i++)
	{
		std::cout << second()[i] << std::endl;
	}
}
