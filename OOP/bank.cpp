﻿#include "stdafx.h"
#include "bank.h"
#include <iostream>


bank::bank(const std::string& name, const std::string& account_number, const int balance)	
{
	name_ = name;
	account_number_ = account_number;
	balance_ = balance;
}

void bank::show() const
{
	std::cout << "Name: " << name_ << std::endl;
	std::cout << "Account: " << account_number_ << std::endl;
	std::cout << "Balance: " << balance_ << std::endl;
}

void bank::deposite(const int amount)
{
	if (amount <= 0)
	{
		std::cout << "You can't deposite negative or equal to zero sum of money." << std::endl;
	} 
	else
	{
		balance_ += amount;
	}
}

void bank::withdraw(const int amount)
{
	if (amount <= 0)
	{
		std::cout << "You can't withdraw negative or equal to zero sum of money." << std::endl;
	}
	else
	{
		balance_ -= amount;
	}
}




