﻿#include "stdafx.h"
#include "stack.h"
#include <iostream>

stack::stack(const int size)
{
	arr_ = new int[size];
	this->capacity_ = size;
	top_ = -1;
}

stack::~stack()
{
	delete[] arr_;
}

void stack::push(const int n)
{
	if (top_  == capacity_ - 1)
	{
		throw std::string("Overflow");
	}
	arr_[++top_] = n;
}

void stack::pop()
{
	if (top_ == -1)
	{
		throw std::string("Is empty.");
	}

	top_--;
}

int stack::peek() const
{
	if (top_ == -1)
	{
		throw std::string("Stack is empty");
	}

	return *arr_;
}

bool stack::is_empty() const
{
	return top_ == -1;
}
