﻿#include "stdafx.h"
#include "queue.h"

queue::queue(int qs) : qsize_(qs)
{
	front_ = rear_ = nullptr;
	items_ = 0;
}

bool queue::is_empty()
{
	return items_ == 0;
}

bool queue::is_full()
{
	return items_ == qsize_;
}

int queue::queue_count() const
{
	return items_;
}

bool queue::enqueue(const int& item)
{
	if (is_full())
	{
		return false;
	}

	auto* add = new node;
	if (add == nullptr)
	{
		return false;
	}
	add->item = item;
	add->next = nullptr;
	items_++;

	if (front_ = nullptr)
	{
		front_ = add;
	}
	else
	{
		rear_->next = add;
	}
	rear_ = add;

	return true;
}

bool queue::dequeue(int& item)
{
	if (front_ == nullptr)
		return false;
	item = front_->item;
	items_--;
	auto* temp = front_;
	front_ = front_->next;
	delete temp;
	if (items_ == 0)
	{
		rear_ = nullptr;
	}

	return true;
}

queue::~queue()
{
	while (front_ != nullptr)
	{
		const auto temp = front_;
		front_ = front_->next;
		delete temp;
	}
}



