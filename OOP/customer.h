﻿#pragma once

class customer
{
	long arrive_;
	int processtime_;
public:
	customer() { arrive_ = processtime_ = 0; }
	void set(long when);
	long when() const { return arrive_; }
	int ptime() const { return processtime_; }

};
