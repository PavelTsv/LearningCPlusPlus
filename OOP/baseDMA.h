﻿#pragma once
#include <ostream>

class baseDMA
{
	char* label_;
	int rating_;
public:
	explicit baseDMA(const char* l = "null", const int r = 0);
	explicit baseDMA(const baseDMA& rs);
	virtual ~baseDMA();
	baseDMA& operator=(const baseDMA& rs);
	friend std::ostream& operator<<(std::ostream& os, const baseDMA& rs);
};

class lacksDMA : public baseDMA
{
	enum {len = 40};
	char color_[len];
public:
	explicit lacksDMA(const char* c = "blank", const char* l = "null", int r = 0);
	explicit lacksDMA(const char* c, const baseDMA& rs);
	friend std::ostream& operator<<(std::ostream& os , const lacksDMA& ls);
};

class hasDMA : public baseDMA
{
	char* style_;
public:
	explicit hasDMA(const char* s = "none", const char* l = "null", int r = 0);
	explicit hasDMA(const char* s, const baseDMA& rs);
	hasDMA(const hasDMA& hs);
	~hasDMA();
	hasDMA& operator=(const hasDMA& rs);
	friend std::ostream& operator<<(std::ostream& os, const hasDMA& rs);
};