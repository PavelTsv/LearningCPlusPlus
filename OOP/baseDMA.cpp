﻿#include "stdafx.h"
#include "baseDMA.h"
#pragma warning(disable : 4996)

baseDMA::baseDMA(const char* l, const int r)
{
	label_ = new char[strlen(l) + 1];
	strcpy(label_, l);
	rating_ = r;
}

baseDMA::baseDMA(const baseDMA& rs)
{
	label_ = new char[strlen(rs.label_) + 1];
	strcpy(label_, rs.label_);
	rating_ = rs.rating_;
}

baseDMA::~baseDMA()
{
	delete[] label_;
}

baseDMA& baseDMA::operator=(const baseDMA& rs)
{
	if (this == &rs)
		return *this;
	delete[] label_;
	label_ = new char[strlen(rs.label_) + 1];
	strcpy(label_, rs.label_);
	rating_ = rs.rating_;
	return *this;
}

std::ostream& operator<<(std::ostream& os, const baseDMA& rs)
{
	os << "Label: " << rs.label_ << "\n";
	os << "Rating: " << rs.rating_ << "\n";
	return os;
}

lacksDMA::lacksDMA(const char* c, const char* l, int r) : baseDMA(l, r)
{
	strncpy(color_, c, 39);
	color_[39] = '\0';
}

lacksDMA::lacksDMA(const char* c, const baseDMA& rs) : baseDMA(rs)
{
	strncpy(color_, c, 39);
	color_[39] = '\0';
}

std::ostream& operator<<(std::ostream& os, const lacksDMA& ls)
{
	os << (const baseDMA&)ls;
	os << "Color: " << ls.color_ << '\0';
	return os;
}

hasDMA::hasDMA(const char* s, const char* l, int r) : baseDMA(l, r)
{
	style_ = new char[strlen(s) + 1];
	strcpy(style_, s);
}

hasDMA::hasDMA(const char* s, const baseDMA& rs) : baseDMA(rs)
{
	style_ = new char[strlen(s) + 1];
	strcpy(style_, s);
}

hasDMA::hasDMA(const hasDMA& hs) : baseDMA(hs)
{
	style_ = new char[strlen(hs.style_) + 1];
	strcpy(style_, hs.style_);
}

hasDMA::~hasDMA()
{
	delete[] style_;
}

hasDMA& hasDMA::operator=(const hasDMA& rs)
{
	if (this == &rs)
		return *this;
	baseDMA::operator=(rs);
	style_ = new char[strlen(rs.style_) + 1];
	strcpy(style_, rs.style_);
}

std::ostream& operator<<(std::ostream& os, const hasDMA& hs)
{
	os << (const baseDMA&)hs;
	os << "Style: " << hs.style_ << "\0";
	return os;
}