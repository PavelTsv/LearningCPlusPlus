﻿#pragma once

class queue
{
	struct node { int item; struct node* next; };
	enum {q_size = 10};
	node* front_;
	node* rear_;
	int items_;
	const int qsize_;
	queue(const queue& q) : qsize_(0) {}
	queue& operator=(const queue& q) { return  *this; }
public:
	queue(int qs = q_size);
	~queue();
	bool is_empty();
	bool is_full();
	int queue_count() const;
	bool enqueue(const int& item);
	bool dequeue(int& item);

};
