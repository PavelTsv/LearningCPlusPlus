﻿#pragma once

class stack
{
	int top_;
	int capacity_;
	int* arr_;
public:
	explicit stack(int size);
	~stack();
	void push(int n);
	void pop();
	int peek() const;
	bool is_empty() const;
};
