﻿#pragma once
#include <ostream>

namespace v
{
	class vect
	{
		double x_;
		double y_;
		double mag_;
		double ang_;
		char mode_;
		void set_mag();
		void set_ang();
		void set_x();
		void set_y();
	public:
		vect();
		vect(double n1, double n2, char mode = 'r');
		~vect();
		void set(double n1, double n2, char mode = 'r');
		double x_val() const { return x_; }
		double y_val() const { return y_; }
		double mag_val() const { return mag_; }
		double ang_val() const { return ang_; }
		void polar_mode();
		void rect_mode();
		vect operator+(const vect& vect) const;
		vect operator-(const vect& vect) const;
		vect operator-() const;
		vect operator*(double n) const;
		friend vect operator*(double n, vect& vect);
		friend std::ostream& operator<<(std::ostream& os, const vect& vect);
	};
	
}
