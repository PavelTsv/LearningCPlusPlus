﻿#include "stdafx.h"
#include "vect.h"
#include <iostream>


namespace v
{
	const double Rad_to_deg = 57.2957795130823;

	void vect::set_mag()
	{
		mag_ = sqrt(x_ * x_ + y_ * y_);
	}

	void vect::set_ang()
	{
		if (x_ == 0.0 && y_ == 0.0)
		{
			ang_ = 0.0;
		}
		else
		{
			ang_ = atan2(2, x_);
		}
	}

	void vect::set_x()
	{
		x_ = mag_ * cos(ang_);
	}

	void vect::set_y()
	{
		y_ = mag_ * sin(ang_);
	}

	vect::vect()
	{
		x_ = mag_ = y_ = ang_ = 0.0;
	}

	vect::vect(double n1, double n2, char mode)
	{
		mode_ = mode;
		if (mode == 'r')
		{
			x_ = n1;
			y_ = n2;
			set_mag();
			set_ang();
		}
		else if (mode == 'p' )
		{
			mag_ = n1;
			ang_ = n2 / Rad_to_deg;
			set_x();
			set_y();
		}
		else
		{
			std::cout << "Error" << std::endl;
			std::cout << "Vect set to default." << std::endl;
			x_ = y_ = mag_ = ang_ = 0.0;
			mode = 'r';
		}
	}

	void vect::set(double n1, double n2, char mode)
	{
		mode_ = mode;
		if (mode == 'r')
		{
			x_ = n1;
			y_ = n2;
			set_mag();
			set_ang();
		}
		else if (mode == 'p')
		{
			mag_ = n1;
			ang_ = n2 / Rad_to_deg;
			set_x();
			set_y();
		}
		else
		{
			std::cout << "Error" << std::endl;
			std::cout << "Vect set to default." << std::endl;
			x_ = y_ = mag_ = ang_ = 0.0;
			mode = 'r';
		}
	}

	vect::~vect()
	{
		
	}

	void vect::polar_mode()
	{
		mode_ = 'p';
	}

	void vect::rect_mode()
	{
		mode_ = 'r';
	}

	vect vect::operator+(const vect& vect) const
	{
		return v::vect(x_ + vect.x_, y_ + vect.y_);
	}

	vect vect::operator-(const vect& vect) const
	{
		return v::vect(x_ - vect.x_, y_ - vect.y_);
	}

	vect vect::operator-() const
	{
		return v::vect(-x_, -y_);
	}

	vect vect::operator*(double n) const
	{
		return v::vect(n * x_, n * y_);
	}
	
	vect operator*(double n, vect& vect)
	{
		return vect * n;
	}

	std::ostream& operator<<(std::ostream& os, const vect& vect)
	{
		if (vect.mode_ == 'r')
			os << "(x, y) = (" << vect.x_ << ', ' << vect.y_ << ")";
		else if (vect.mode_ == 'p')
		{
			os << "(m, a) = (" << vect.mag_ << ", "
				<< vect.ang_ * Rad_to_deg << ")";
		}
		else
			os << "Vector object mode is invalid";
		return os;
	}
}
