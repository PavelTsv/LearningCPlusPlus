﻿#pragma once
#include <string>
#include "Pair.h"
#include <valarray>
typedef std::valarray<int> a_int;
typedef Pair<a_int, a_int> p_arr;

class Wine : private p_arr, std::string
{
	int number_of_years_;
public:
	Wine(const std::string name, const int years, const int year[], const int bottle[]);
	Wine(const std::string name, const int year);
	void get_bottles();
	std::string& name();
	int sum() { return second().sum(); }
	void show() const;
};
