﻿#include "stdafx.h"
#include "cd.h"
#include <iostream>
#pragma warning(disable : 4996)

cd::cd(const char* s1, const char* s2, int n, double x)
{
	strncpy_s(performance_, s1, 49);
	performance_[49] = '\0';
	strncpy_s(label_, s2, 19);
	label_[19] = '\0';
	selection_ = n;
	play_time_ = x;
}
cd::~cd()
{
	
}


void cd::report() const
{
	std::cout << "Perfomance: " << performance_ << "\n";
	std::cout << "Label: " << label_ << "\n";
	std::cout << "Selection: " << selection_ << "\n";
	std::cout << "Play time: " << play_time_ << "\n";
}

classic::classic(char* id, char* s1, char* s2, int n, double x) : cd(s1, s2, n, x)
{
	id_ = new char[strlen(id) + 1];
	strcpy(id_, id);
}

classic::classic(const classic& c) : cd(c)
{
	id_ = new char[strlen(c.id_) + 1];
	strcpy(id_, c.id_);
}

classic::~classic()
{
	delete[] id_;
}

void classic::report() const
{
	cd::report();
	std::cout << "Id: " << id_ << "\n";
}

classic& classic::operator=(const classic& c)
{
	if (this == &c)
		return *this;
	delete[] id_;
	cd::operator=(c);
	id_ = new char[strlen(c.id_) + 1];
	strcpy(id_, c.id_);
	return *this;
}



