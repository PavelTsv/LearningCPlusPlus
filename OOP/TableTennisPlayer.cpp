﻿#include "stdafx.h"
#include "TableTennisPlayer.h"
#include <iostream>

TableTennisPlayer::TableTennisPlayer(const char* fn, const char* ln, const bool ht)
{
	strncpy_s(first_name_, fn, lim - 1);
	first_name_[lim - 1] = '\0';
	strncpy_s(last_name_, ln, lim - 1);
	last_name_[lim - 1] = '\0';
	has_table_ = ht;
}

void TableTennisPlayer::name() const
{
	std::cout << last_name_ << ", " << first_name_ << "\n";
}


RatedPlayer::RatedPlayer(unsigned r, const char* fn, const char* ln, const bool ht) : TableTennisPlayer(fn, ln, ht)
{
	rating_ = r;
}

RatedPlayer::RatedPlayer(unsigned r, const TableTennisPlayer& tp) : TableTennisPlayer(tp)
{
	rating_ = r;
}
