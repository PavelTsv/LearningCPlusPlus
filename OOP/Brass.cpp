﻿#include "stdafx.h"
#include "Brass.h"
#include <cstring>
#include <iostream>

Brass::Brass(const char* s, long an, double bal)
{
	strncpy_s(full_name_, s, max - 1);
	full_name_[max - 1] = '\0';
	acc_num_ = an;
	balance_ = bal;
}
Brass::~Brass()
{
	
}


void Brass::deposit(double amt)
{
	if (amt < 0)
	{
		std::cout << "negataive deposit.";
	}
	else
	{
		balance_ += amt;
	}
}

void Brass::withdraw(double amt)
{
	if (amt < 0)
	{
		std::cout << "negative withdraw.";
	}
	else if (amt <= balance_)
	{
		balance_ -= amt;
	}
	else
	{
		std::cout << "exceeds balance.";
	}
}

void Brass::view_acc() const
{
	// set up ###.## format
	std::ios_base::fmtflags initialState =
		std::cout.setf(std::ios_base::fixed, std::ios_base::floatfield);
	std::cout.setf(std::ios_base::showpoint);
	std::cout.precision(2);
	std::cout << "Client: " << full_name_ << std::endl;
	std::cout << "Account Number : " << acc_num_ << std::endl;
	std::cout << "Balance: $" << balance_ << std::endl;
	std::cout.setf(initialState); // restore original format
}

BrassPlus::BrassPlus(const char* s, long an, double bal, double ml, double r) : Brass(s, an, bal)
{
	max_loan_ = ml;
	owes_bank_ = 0.0;
	rate_ = r;
}

BrassPlus::BrassPlus(const Brass& br, double ml, double r) : Brass(br)
{
	max_loan_ = ml;
	owes_bank_ = 0.0;
	rate_ = r;
}

void BrassPlus::view_acc() const
{
	std::ios_base::fmtflags initialState =
		std::cout.setf(std::ios_base::fixed, std::ios_base::floatfield);
	std::cout.setf(std::ios_base::showpoint);
	std::cout.precision(2);
	Brass::view_acc(); // display base portion
	std::cout << "Maximum loan : $" << max_loan_ << std::endl;
	std::cout << "Owed to bank : $" << owes_bank_ << std::endl;
	std::cout << "Loan Rate : " << 100 * rate_ << "%\n";
	std::cout.setf(initialState);
}

void BrassPlus::withdraw(double amt)
{
	std::ios_base::fmtflags initialState =
		std::cout.setf(std::ios_base::fixed, std::ios_base::floatfield);
	std::cout.setf(std::ios_base::showpoint);
	std::cout.precision(2);
	double bal = balance();
	if (amt <= bal)
		Brass::withdraw(amt);
	else if (amt <= bal + max_loan_ - owes_bank_)
	{
		double advance = amt - bal;
		owes_bank_ += advance * (1.0 + rate_);
		std::cout << "Bank advance : $" << advance << std::endl;
		std::cout << "Finance charge : $" << advance * rate_ << std::endl;
		deposit(advance);
		Brass::withdraw(amt);
	}
	else
		std::cout << "Credit limit exceeded.Transaction cancelled.\n";
	std::cout.setf(initialState);
}
