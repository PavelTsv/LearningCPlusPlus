﻿#pragma once
#include <string>

class bank
{

private:
	std::string name_;
	std::string account_number_;
	int balance_;
public:
	bank(const std::string& name, const std::string& account_number, const int balance);
	void show() const;
	void deposite(const int amount);
	void withdraw(const int amount);

};
