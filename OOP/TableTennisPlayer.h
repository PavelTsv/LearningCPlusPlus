﻿#pragma once

class TableTennisPlayer
{
	enum {lim = 20};
	char first_name_[lim];
	char last_name_[lim];
	bool has_table_;
public:
	explicit TableTennisPlayer(const char* fn = "none", const char* ln = "none", const bool ht = false);
	void name() const;
	bool has_table() const { return has_table_; }
	void reset_table(const bool v) { has_table_ = v; }
	
};

class RatedPlayer : TableTennisPlayer
{
	unsigned int rating_;
public:
	explicit RatedPlayer(unsigned int r = 0, const char* fn = "none", const char* ln = "none", const bool ht = false);
	RatedPlayer(unsigned int r, const TableTennisPlayer& tp);
	unsigned int rating() const { return rating_; }
	void reset_rating(unsigned int r) { rating_ = r; }
};