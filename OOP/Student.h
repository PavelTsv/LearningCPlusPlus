﻿#pragma once
#include <string>
#include <valarray>

class Student
{
	typedef std::valarray<double> d_arr;
	std::string name_;
	d_arr scores_;
	std::ostream& print(std::ostream& os) const;
public:
	Student() : name_("Null"), scores_() {}
	explicit Student(const std::string& s) : name_(s), scores_() {}
	explicit Student(const int n) : name_("Null"), scores_(n) { }
	Student(const std::string& s, const int n) : name_(s), scores_(n) {}
	Student(const std::string& s, const d_arr& d_arr) : name_(s), scores_(d_arr) {}
	Student(const char* s, const double* pd, const int n) : name_(s), scores_(pd, n) {}
	~Student() {}
	double average() const;
	const std::string& name() const;
	double& operator[](const int i);
	double operator[](const int i) const;
	friend std::istream& operator>>(std::istream& is, Student& st);
	friend std::istream& getline(std::istream& is, Student& st);
	friend std::ostream& operator<<(std::ostream& os, const Student& st);
};
